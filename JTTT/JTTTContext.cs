﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class JTTTContext: DbContext
    {
        public JTTTContext()
            : base("JTTTDb3")
        {
            Database.SetInitializer(new JTTTInitializer());
        }

        public DbSet<Task> Task { get; set; }
        public DbSet<Action> Action { get; set; }
        public DbSet<Condition> Condition { get; set; }
    }
}
