﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace JTTT
{
    class TasksList
    {
        BindingList<Task> tasks;
        string datafile;

        public BindingList<Task> Tasks
        {
            get { return tasks; }
        }
        public TasksList(string initdfile)
        {
            datafile = initdfile;
            tasks = new BindingList<Task>();
        }

        public void Serialize()
        {
            FileManager.Remove(datafile);
            Stream stream = File.Open(datafile, FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, tasks);
            stream.Close();
        }

        public void Deserialize()
        {
            if (File.Exists(@datafile))
            {
                Stream stream = File.Open(datafile, FileMode.Open);

                BinaryFormatter formatter = new BinaryFormatter();
                tasks.Clear();
                BindingList<Task> loaded = (BindingList<Task>)formatter.Deserialize(stream);

                foreach (Task t in loaded)
                {
                    tasks.Add(t);
                }

                stream.Close();
            }      
        }

        public void AddToList(Task t)
        {
            tasks.Add(t);
        }

        public void ExecuteAll()
        {
            foreach (Task t in tasks)
            {
                t.CheckAndExecute();
            }
        }

        public void Clear()
        {
            tasks.Clear();
        }
    }
}
