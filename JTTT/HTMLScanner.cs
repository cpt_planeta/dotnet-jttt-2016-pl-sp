﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;

namespace JTTT
{
    class HTMLScanner
    {
        //Get html from given url and return as a string.
        private string GetPageHtml(string url)
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(url));

                return html;
            }
        }


        //Returns urls to images
        public string[] GetImgsrcs(string phrase, string url)
        {
            //Check if link start with http://
            string correct_url = url.StartsWith("http://") ? url : "http://" + url;
            List<string> imgurls = new List<string>();
            string alt;
            string imgsrc;
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml(correct_url);
            doc.LoadHtml(pageHtml);
            //Img nodes
            var nodes = doc.DocumentNode.Descendants("img");

            //Iterate over all nodes
            foreach (var node in nodes)
            {
                //Get alt of image
                alt = node.GetAttributeValue("alt", "");
                //Get src of image
                imgsrc = node.GetAttributeValue("src", "");
                //Sometimes have to correct url
                imgsrc = imgsrc.StartsWith("/") ? url.Substring(0, url.Length - 1) + imgsrc : imgsrc;
                //Check if alt contains phrase. Ignore letter case.
                if(alt.ToLower().Contains(phrase.ToLower()))
                {
                    imgurls.Add(imgsrc);
                }
            }
            //Return array of image urls
            return imgurls.ToArray();
        }
    }
}
