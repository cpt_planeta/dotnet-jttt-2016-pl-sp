﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace JTTT
{
    //Class for managing files - downloading, removing
    class FileManager
    {
        //Download a file from a given source and return filepath
        public static string Download(string source)
        {
            string filepath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".jpg";
            Console.Write(filepath);

            using (WebClient client = new WebClient())
            {
                client.DownloadFile(source, filepath);
            }

            return filepath;
        }

        //Download files from a given sources and return array of filepaths
        public static string[] Download(string[] source)
        {
            List<string> filepaths = new List<string>();

            foreach (var src in source)
            {
                filepaths.Add(Download(src));
            }

            return filepaths.ToArray();
        }

        //Remove file at given filepath. Returns true if success, false on failure.
        public static void Remove(string filepath)
        {
            try
            {
                if (File.Exists(@filepath))
                {
                    File.Delete(@filepath);
                }
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public static void Remove(string[] filepaths)
        {
            foreach (var filepath in filepaths)
                try
                {
                    if (File.Exists(@filepath))
                    {
                        File.Delete(@filepath);
                    }
                }
                catch (IOException ex)
                {
                    throw ex;
                }
        }
        
    }
}
