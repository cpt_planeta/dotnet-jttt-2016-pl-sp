﻿namespace JTTT
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urlBox = new System.Windows.Forms.TextBox();
            this.doButton = new System.Windows.Forms.Button();
            this.phraseBox = new System.Windows.Forms.TextBox();
            this.mailBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listaZadan = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // urlBox
            // 
            this.urlBox.AccessibleName = "";
            this.urlBox.BackColor = System.Drawing.SystemColors.Info;
            this.urlBox.Location = new System.Drawing.Point(12, 98);
            this.urlBox.Name = "urlBox";
            this.urlBox.Size = new System.Drawing.Size(248, 20);
            this.urlBox.TabIndex = 2;
            this.urlBox.Text = "http://demotywatory.pl/";
            // 
            // doButton
            // 
            this.doButton.Location = new System.Drawing.Point(75, 218);
            this.doButton.Name = "doButton";
            this.doButton.Size = new System.Drawing.Size(101, 41);
            this.doButton.TabIndex = 5;
            this.doButton.Text = "Dodaj do listy";
            this.doButton.UseVisualStyleBackColor = true;
            this.doButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // phraseBox
            // 
            this.phraseBox.BackColor = System.Drawing.SystemColors.Info;
            this.phraseBox.Location = new System.Drawing.Point(12, 137);
            this.phraseBox.Name = "phraseBox";
            this.phraseBox.Size = new System.Drawing.Size(248, 20);
            this.phraseBox.TabIndex = 3;
            // 
            // mailBox
            // 
            this.mailBox.BackColor = System.Drawing.SystemColors.Info;
            this.mailBox.Location = new System.Drawing.Point(12, 176);
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(248, 20);
            this.mailBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 111;
            this.label1.Text = "Jeśli na stronie:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 13);
            this.label2.TabIndex = 111;
            this.label2.Text = "są obrazki zawierające w opisie frazę:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 111;
            this.label3.Text = "to prześlij je na maila:";
            // 
            // listaZadan
            // 
            this.listaZadan.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listaZadan.FormattingEnabled = true;
            this.listaZadan.Location = new System.Drawing.Point(317, 25);
            this.listaZadan.Name = "listaZadan";
            this.listaZadan.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listaZadan.Size = new System.Drawing.Size(528, 134);
            this.listaZadan.TabIndex = 111;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(317, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Wykonaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(398, 169);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 32);
            this.button2.TabIndex = 7;
            this.button2.Text = "Czyść";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(770, 169);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 32);
            this.button3.TabIndex = 9;
            this.button3.Text = "Deserialize";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(689, 169);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 32);
            this.button4.TabIndex = 8;
            this.button4.Text = "Serialize";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(13, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 18);
            this.label4.TabIndex = 111;
            this.label4.Text = "Nazwa zadania";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(12, 35);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(248, 20);
            this.nameBox.TabIndex = 1;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 288);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listaZadan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mailBox);
            this.Controls.Add(this.phraseBox);
            this.Controls.Add(this.doButton);
            this.Controls.Add(this.urlBox);
            this.Name = "Window";
            this.Text = "JTTT v1.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox urlBox;
        private System.Windows.Forms.TextBox phraseBox;
        private System.Windows.Forms.TextBox mailBox;
        private System.Windows.Forms.Button doButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listaZadan;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label4;
        
    }
}

