﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    class ImgOnPage : Condition
    {
        public string phrase { get; set; }
        public string url { get; set; }

        [Obsolete("Only needed for serialization and materialization", true)]
        public ImgOnPage(){}
        public ImgOnPage(string initphrase, string initurl)
        {
            phrase = initphrase;
            url = initurl;
        }

        public override bool Check()
        {
            HTMLScanner scan = new HTMLScanner();
            string[] imgsrcs = scan.GetImgsrcs(phrase, url);
            if (imgsrcs == null || imgsrcs.Length == 0)
            {
                return false;
            }
            return true;
        }
    }
}
