﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;

namespace JTTT
{
    public class GMailSend
    {
        SmtpClient smtpClient;
        string myMail;
        string myName;
        public GMailSend(string address, string name, string pass) {
            myMail = address;
            myName = name;
            smtpClient = new SmtpClient(); //tworzymy klienta smtp
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;
            smtpClient.Host = "smtp.gmail.com"; //host serwera
            // hardcoded
            smtpClient.Credentials = new System.Net.NetworkCredential(address, pass);//adres i hasło nadawcy
        }
        public void SendMessageAndAttachment(string subject, string body, string[] emails, string[] filepaths)
        {
            MailMessage message = new MailMessage();//tworzymy wiadomość
            MailAddress from = new MailAddress(myMail, myName);//adres nadawcy i nazwa nadawcy
            message.From = from;
            foreach (string addr in emails)
            {
                message.To.Add(addr);//adres odbiorcy
            }
            message.Subject = subject;//temat wiadomości
            message.Body = body; //treść wiadomości
            foreach (string att in filepaths)
            {
                message.Attachments.Add(new Attachment(@att));
            }
            
            try
            {
                smtpClient.Send(message);//nazwa odbiorcy, wysyłamy wiadomość
            }
            catch (SmtpException ex)
            {

                throw new ApplicationException("Klient SMTP wywołał wyjątek. Sprawdź połączenie z internetem." + ex.Message);

            }
            finally
            {
                foreach (Attachment att in message.Attachments)
                {
                    att.Dispose();
                }
            }        
        }
    }
}
