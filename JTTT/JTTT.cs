﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT
{
    class JTTT
    {
        TasksList alltasks;
        public JTTT()
        {
            alltasks = new TasksList("data");
            using (var ctx = new JTTTContext())
            {
                foreach (var t in ctx.Task)
                {
                    t.action = ctx.Action.Find(t.ActionId);
                    t.condition = ctx.Condition.Find(t.ConditionId);
                    alltasks.AddToList(t);
                }
            }
        }

        public BindingList<Task> Tasks
        {
            get { return alltasks.Tasks; }
        }
        public void ExecuteAll()
        {
            Log.Write("Executing all Tasks from list");
            alltasks.ExecuteAll();

            System.Windows.Forms.MessageBox.Show("Done!");
        }

        public void AddToList(string name, string phrase, string url, string email)
        {
            Log.Write("Adding Task to list");
            SendImgFromPage t = new SendImgFromPage(name, phrase, url, "Your daily dose of images!", "", email);
            alltasks.AddToList(t);

            using (var ctx = new JTTTContext())
            {
                ctx.Task.Add(t);
                ctx.Action.Add(t.action);
                ctx.Condition.Add(t.condition);
                ctx.SaveChanges();
            }
        }

        public void SerializeAll()
        {
            Log.Write("Serializing");
            alltasks.Serialize();

            System.Windows.Forms.MessageBox.Show("Serialized!");
        }

        public void Deserialize()
        {
            Log.Write("Deserializing");
            alltasks.Deserialize();

            using (var ctx = new JTTTContext())
            {
                foreach (var t in alltasks.Tasks)
                {
                    ctx.Task.Add(t);
                    ctx.Action.Add(t.action);
                    ctx.Condition.Add(t.condition);
                    ctx.SaveChanges();
                }
            }

            System.Windows.Forms.MessageBox.Show("Loaded!");
        }

        public void Clear()
        {
            Log.Write("Clearing Tasks list");
            alltasks.Clear();
            using (var ctx = new JTTTContext())
            {
                foreach (var t in ctx.Task)
                {
                    ctx.Task.Remove(t);
                }

                foreach (var a in ctx.Action)
                {
                    ctx.Action.Remove(a);
                }

                foreach (var c in ctx.Condition)
                {
                    ctx.Condition.Remove(c);
                }
            
                ctx.SaveChanges();
            }
        }
    }
}
