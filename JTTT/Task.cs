﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    public abstract class Task
    {
        public int Id { get; set; }
        public int ActionId { get; set; }
        public int ConditionId { get; set; }

        public string name { get; set; }
        public virtual Action action { get; set; }
        public virtual Condition condition { get; set; }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Task() { }

        public Task(string initname)
        {
            name = initname;
        }
        public bool CheckAndExecute()
        {
            if (condition.Check())
            {
                action.Execute();
                return true;
            }
            return false;
        }

        public override abstract String ToString();
    }
}
