﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    class SendImgFromPage : Task
    {
        public string phrase { get; set; }
        public string url { get; set; }
        public string subj { get; set; }
        public string body { get; set; }
        public string email { get; set; }
        public SendImgFromPage(string initname, string initphrase, string initurl, string initsubj, string initbody, string initemail)
            : base(initname)
        {
            phrase = initphrase;
            url = initurl;
            subj = initsubj;
            body = initbody;
            email = initemail;
            condition = new ImgOnPage(phrase, url);
            action = new DownloadAndSendImages(subj, body, email, phrase, url);
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public SendImgFromPage(){}

        public override String ToString()
        {
            string display = "Nazwa: " + name + " | Do: " + email + " | Fraza: " + phrase + " | Strona: " + url;

            return display;
        }
    }
}
