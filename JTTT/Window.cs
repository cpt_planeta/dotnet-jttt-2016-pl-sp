﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Window : Form
    {
        JTTT manager;
        public Window()
        {
            InitializeComponent();
            manager = new JTTT();
            listaZadan.DataSource = manager.Tasks;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Pass arguments from boxes to DoIt method.
            manager.AddToList(nameBox.Text ,phraseBox.Text, urlBox.Text, mailBox.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            manager.SerializeAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            manager.Deserialize();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            manager.Clear();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            manager.ExecuteAll();
        }
    }
}
