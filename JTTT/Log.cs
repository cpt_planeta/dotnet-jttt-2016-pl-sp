﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace JTTT
{
    class Log
    {
        public static void Write(string logMessage = "")
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            using (StreamWriter w = File.AppendText("jttt.log"))
            {
                w.WriteLine("{0} {1} ({2}): {3}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString(), sf.GetMethod().Name, logMessage);
            }

        }
    }
}
