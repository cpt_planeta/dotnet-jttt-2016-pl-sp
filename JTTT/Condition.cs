﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    public abstract class Condition
    {
        public int Id { get; set;}
        public abstract bool Check();
    }
}

