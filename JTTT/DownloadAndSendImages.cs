﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    

    [Serializable]
    class DownloadAndSendImages : Action
    {
        public string subj { get; set; }
        public string body { get; set; }
        public string email { get; set; }
        public string phrase { get; set; }
        public string url { get; set; }

        [Obsolete("Only needed for serialization and materialization", true)]
        public DownloadAndSendImages() { }
        public DownloadAndSendImages(string initsubj, string initbody, string initemail, string initphrase, string initurl)
        {
            subj = initsubj;
            body = initbody;
            email = initemail;
            phrase = initphrase;
            url = initurl;
        }

        public override void Execute()
        {
            HTMLScanner html = new HTMLScanner();
            string[] imgsrcs = html.GetImgsrcs(phrase, url);

            string[] attachments = FileManager.Download(imgsrcs);

            GMailSend client = new GMailSend("jtttfinal@gmail.com", "JTTTv1.0", "jkeqqhrditnfdelx");
            client.SendMessageAndAttachment(subj, body, new string[] { email }, attachments);

            FileManager.Remove(attachments);
        }
    }
}
